///////////////////////////////////////////////
React Native Live Templates for WebStorm
///////////////////////////////////////////////

1. open preferences
2. editor -> live templates
3. add template group for React Native
4. add templates below to the new group
5. define context -> javascript
6. edit variables -> add "fileNameWithoutExtension" to "$fnName$"


///////////////////////////////////////////////

Templates

***********************************************
rncc: react native statefull component
***********************************************

import React, { Component } from 'react';
import { View } from 'react-native';

class $fnName$ extends Component() {
  propTypes = {}
  state = {};
  
  render() {
    return (
      
    )
  }
}

***********************************************
rncf: react native functional component
***********************************************

import React from 'react';
import { View } from 'react-native';

$fnName$.propTypes = {

}

export const $fnName$ = () => {
  return (
    
  )
};

***********************************************
rnrs: React Native Redux Store
***********************************************

export function dummieDispatch (state) {
  return {
    type: TEST,
    state,
  }
}

export function dummieAction () {
  return function (dispatch, getState) {
    dispatch(dummieDispatch())
  }
}



const initialState = {
 
}

export default function settings (state = initialState, action) {
  switch (action.type) {
    case TEST :
      return {
        ...state,
      }
    default :
      return state
  }
}

***********************************************
rnss: React Native Stylesheet
***********************************************

import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  

});

export default styles;

***********************************************